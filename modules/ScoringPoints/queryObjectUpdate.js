/**
 * Updates the query object and writes it to state.query
 *
 * @since 0.0.1
 * @memberof queryObjectUpdate
 * @param {Object} state - object with app state
 * @param {Number} number - quotient from cells
 * @param {Number} i - cell number
 */
function createsQuery (state, number, i) {
  state.query = {
    cmd: 'confirm',
    eventId: state.eventId,
    _reqId: state._reqId,
    div: number,
    n: i,
    set: state.set
  }
}

/**
 * Calculates the quotient of cells and sets the primary cell
 *
 * @since 0.0.1
 * @memberof queryObjectUpdate
 * @param {Object} state - object with app state
 * @param {Number} num1 - first cell value
 * @param {Number} num2 - second cell value
 * @return {Number|Boolean} - quotient from cells or false
 */
function calculatesQuotientCell (state, num1, num2) {
  if (state.min < num1 / num2 && num1 / num2 < state.max) {
    state.set = 'set1'
    return num1 / num2
  }

  if (state.min < num2 / num1 && num2 / num1 < state.max) {
    state.set = 'set2'
    return num2 / num1
  }

  return false
}

/**
 * Updates the state.query with new data
 *
 * @since 0.0.1
 * @namespace queryObjectUpdate
 * @param {Object} state - object with app state
 * @param {Object} data - data for updating "state"
 * @param {Array} data.set1 - array of values
 * @param {Array} data.set2 - array of values
 * @param {Number} [i = 0] - cell number in the array
 * @example
 *
 * const state = {
 *      min:1,
 *      max:1.4,
 *      eventId:'example',
 *      _reqId:'example',
 *      query:null
 * }
 *
 * queryObjectUpdate (state,{[100,200.300],[80,200,300]})
 *
 * //state.query ={cmd: 'confirm',
 * //   eventId: 'example',
 * //   _reqId: 'example',
 * //   div: 1.25,
 * //   n: 0,
 * //   set: 'set1'
 * //}
 */
function queryObjectUpdate (state, { set1, set2 }, i = 0) {
  if (i > set1.length || i > set2.length) {
    return false
  }

  const result = calculatesQuotientCell(state, set1[i], set2[i])

  result ? createsQuery(state, result, i) : queryObjectUpdate(state, { set1, set2 }, i + 1)
}

module.exports = queryObjectUpdate
