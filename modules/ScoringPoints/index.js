const createsToken = require('./createsToken')
const queryObjectUpdate = require('./queryObjectUpdate')

/**
 * Provides a "start" method that scores the indicated number of points
 *
 * @since 0.0.1
 * @namespace ScoringPoints
 */
class ScoringPoints {
  /**
   *
   * @memberof ScoringPoints
   * @param {Object} api - Object with methods.
   * @param {Object} api.send - Sends messages
   * @param {Object} api.listen - Listening to incoming messages
   * @param {Object} api.close - Closes the connection
   */
  constructor ({ send, listen, close }) {
    this.send = send
    this.listen = listen
    this.close = close
    this.state = {
      point: 0,
      failed: 0
    }
  }

  /**
   * Asynchronous stream delay
   *
   * @since 0.0.1
   * @memberof ScoringPoints
   * @param {Number} time - The number of seconds to delay
   * @return {Promise} - Promise will be fulfilled after delay
   */
  sleep (time) {
    return new Promise(resolve => setTimeout(() => resolve(), time * 1000))
  }

  /**
    * Request cell data
    *
    * @since 0.0.1
    * @memberof ScoringPoints
    * @param {Object} data - data object
    * @param {Object} data.events - object with event id
    */
  requestDataCells (data) {
    Object.keys(data.events).forEach(el => {
      this.state.eventId = el
      this.send({ cmd: 'getData', eventId: el, _reqId: data._reqId })
    })
  }

  /**
   * Parses the server response and processes it depending on the content
   *
   * @since 0.0.1
   * @memberof ScoringPoints
   * @param {Object} data - server response object
   */
  router (data) {
    if (data.events) {
      this.state.min = data.minDiv
      this.state.max = data.maxDiv
      this.requestDataCells(data)
    } else if (data.status && data.data) {
      queryObjectUpdate(this.state, data.data)
      this.send(this.state.query)
    } else if (data.status && data.message === 'Success') {
      this.send(this.state.query)
    } else if (data.cmd === 'points') {
      this.state.point = data.points
      console.log(data.points)
    } else if (!data.status && data.message === 'Failed') {
      this.state.failed += 1
    } else if (data.status && data.message === 'Win') {
      console.log(`Win!!!\nFailed: ${this.state.failed}`)
      this.close()
    } else if (data.message !== 'Hi') {
      throw new Error(`Unknown Type Response: ${data}`)
    }
  }

  /**
   * Processing incoming messages.
   *
   * @since 0.0.1
   * @memberof ScoringPoints
   * @param {JSON} data - valid json
   */
  incomingMessageHandler (data) {
    try {
      this.router(JSON.parse(data))
    } catch (err) {
      console.log(err)
    }
  }

  /**
   * Gaining points on demoserver.dev
   * Displays the number of points scored
   * At the end, the number of errors made is displayed.
   *
   * @since 0.0.1
   * @memberof ScoringPoints
   * @param {Number} [points = 100]
   * @example
   *
   * start()
   *
   * // 1
   * // 2
   * // ...
   * // 99
   * // 100
   * // Win!!!
   * // Failed: 5
   */
  async start (points = 100) {
    const func = this.incomingMessageHandler.bind(this)

    this.listen(func)

    while (this.state.point < points) {
      this.state._reqId = createsToken()
      this.send({ cmd: 'getEvents', _reqId: this.state._reqId })

      if (!this.state.start) {
        this.send({ cmd: 'getPoints', _reqId: this.state._reqId })
        this.state.start = true
      }

      await this.sleep(2)
    }
  }
}

module.exports = ScoringPoints
