/**
   * Creates a figure
   *
   * @since 0.0.1
   * @memberof createsToken
   * @param {Number} [num = 16] - The number system in which the digit will be created
   * @return {String} - Digit string representation
   */
function createsRandomFigure (num = 16) {
  const number = Math.floor(Math.random() * num + 1)

  return number % num !== 0 ? number.toString(num) : createsRandomFigure(num)
}

/**
    * Generates a token from digits in hexadecimal notation
    * and hyphens from an array
    *
    * @since 0.0.1
    * @namespace createsToken
    * @param {Array} mask - indicating the number of continuous digits
    * @return {String} - Token
    * @example
    *
    * createsToken([8, 4, 4, 4, 12])
    * //=> '2f631cb0-fc18-11e9-8ef2-4575c2f4d9ae'
    * createsToken([4,4])
    * //=> 'fc18-11e9'
    */
function createsToken (mask = [8, 4, 4, 4, 12]) {
  return mask
    .map(el => [...Array(el)].map(() => createsRandomFigure()).join(''))
    .join('-')
}

module.exports = createsToken
