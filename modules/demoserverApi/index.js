/**
 * API For WebSocket. Returns a promise that is executed when the socket opens
 *
 * @since 0.0.1
 * @namespace demoserverApi
 * @param {Object} ws - Web Socket Object
 * @return {Promise} - Object with methods: {send, listen, close}
 */
function demoserverApi (ws) {
  /**
   * Sends messages
   *
   * @since 0.0.1
   * @memberof demoserverApi
   * @param {*} data - data to send.
   */
  function send (data) {
    ws.send(JSON.stringify(data))
  }

  /**
   * Listening to incoming messages
   *
   * @since 0.0.1
   * @memberof demoserverApi
   * @param {Function} callback - Executed when a message is received.
   */
  function listen (callback) {
    ws.on('message', callback)
  }

  /**
   * Closes the connection
   *
   * @since 0.0.1
   * @memberof demoserverApi
   */
  function close () {
    ws.close()
  }

  return new Promise(resolve =>
    ws.on('open', () => {
      console.log('ws connect')
      resolve({
        send,
        listen,
        close
      })
    })
  )
}

module.exports = demoserverApi
