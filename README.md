# Scoring poibts

-   Version 0.0.1

If you are looking for the English version, just scroll down

Данный проект является тестовым заданием и в нем проигнорированы некоторые огнаничения:

1. Не как не учитывается превышения количества запросов и возможность получить 429 ошибку.
2. Поле "\_reqId" генерируется случайным образом (совпадает только маска). Скорее всего поле
   являлось ключем и было бы защищено от такого простого подбора.
3. Поле "eventId" достается по ключу объекта, а не по полю ID в нем.

Я буду благодарен за любую обратную связь.

This project is a test task, and some limitations were ignored.

1. Exceeding the number of requests and the ability to get 429 errors.
2. Field "\ \_reqId" randomly generated. This is probably the key and it is protected from such a
   simple selection.
3. The eventId field is obtained by the object key, and not by the ID field in it.

I would be grateful for any feedback.
