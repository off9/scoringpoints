const WebSocket = require('ws')
const ApiServer = require('./modules/demoserverApi')
const ScoringPoints = require('./modules/ScoringPoints')

;(async () => {
  const ws = new WebSocket('wss://demoserver.dev/ws')
  const api = await ApiServer(ws)
  const server = new ScoringPoints(api)

  server.start(100)
})()
